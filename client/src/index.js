import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Dashboard from "./components/dashboard"

import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter, Route, Switch } from "react-router-dom";
ReactDOM.render(
  <BrowserRouter>
      <Route exact path="/" component={App} />
      <Route path="/dashboard" component={Dashboard}/>
  </BrowserRouter>,
  document.getElementById("root")
);
registerServiceWorker();
